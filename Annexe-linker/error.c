#include <stdlib.h>
#include <stdio.h>
#include "error.h"

/**
 * @file 
 * @brief La fonction \p error
 * @author F. Boulier
 * @date novembre 2010
 */

/**
 * @brief Imprime un message sur la sortie d'erreur standard et arrête
 * le processus avec \p exit \p (1). 
 * Appel type : \p error \p (mesg, __FILE__, __LINE__)
 * @param[in] mesg le message
 * @param[in] file le nom du fichier
 * @param[in] line le numéro de ligne
 */

void error (char* mesg, char* file, int line)
{
    fprintf (stderr, "error in %s:%d\n%s\n", file, line, mesg);
    exit (1);
}

