#include <stdio.h>

#define N 1000
int main ()
{   int pred, succ, i, nbcomp_unit, nbcomp_tot, tmp;

    pred = 1;
    succ = 2;
    nbcomp_unit = 1;
    nbcomp_tot = 0;
    printf ("# nb get | nb comparaisons | hauteur AVL\n");
    for (i = 1; i < N; i++)
    {	if (i >= succ)
	{   tmp = succ;
	    succ = pred + succ + 1;
	    pred = tmp;
	    nbcomp_unit += 1;
	}
	nbcomp_tot += nbcomp_unit;
	printf ("%d\t%d\t%d\n", i, nbcomp_tot, nbcomp_unit);
    }
    return 0;
}
