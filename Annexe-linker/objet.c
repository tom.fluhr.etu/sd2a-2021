#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include "error.h"
#include "objet.h"

/**
 * @file
 * @brief Implantation des tables de symboles des fichiers objets
 * @author F. Boulier
 * @date novembre 2010
 */

/**
 * @brief Constructeur. 
 * Initialise O. Cette fonction devrait être appelée avant toute autre
 * utilisation de O.
 * @param[out] O un objet
 */

void init_objet (struct objet* O)
{
    O->filename = (char*)0;
    O->size = 0;
    O->alloc = 0;
    O->tab = (struct symbole*)0;
}

/**
 * @brief Destructeur.
 * Libère les ressources consommées par O. Cette fonction devrait
 * être appelée après la dernière utilisation de O.
 * @param[out] O un objet
 */

void clear_objet (struct objet* O)
{   int i;

    if (O->filename)
	free (O->filename);
    if (O->tab)
    {	for (i = 0; i < O->alloc; i++)
	    clear_symbole (&O->tab [i]);
	free (O->tab);
    }
}

static void realloc_objet (struct objet* O, int n)
{
    if (O->alloc < n)
    {	O->tab = (struct symbole*)realloc
				(O->tab, n * sizeof (struct symbole));
	if (O->tab == NULL)
	    error ("realloc_objet", __FILE__, __LINE__);
    }
    while (O->alloc < n)
    {	init_symbole (&O->tab [O->alloc]);
	O->alloc += 1;
    }
}

/**
 * @brief Charge la table des symboles de \p filename dans O.
 * Les informations sont lues sur le fichier \p f.
 * @param[out] O un objet
 * @param[in] filename un nom de fichier
 * @param[in] f un fichier ouvert
 */

void charger_objet (struct objet* O, char* filename, FILE* f)
{   
#define BUFSIZE 256
    static char buffer [BUFSIZE];
    char* line;
    char* ident;
    char type;
    int i;

    O->filename = (char*)realloc (O->filename, strlen (filename) + 1);
    if (O->filename == (char*)0)
	error ("charger_objet", __FILE__, __LINE__);
    strcpy (O->filename, filename);

    O->size = 0;
    line = fgets (buffer, BUFSIZE-1, f);
    while (line != (char*)0 && line [1] != '\0')
    {	i = 0;
	while (isalnum (line [i]))
	    i += 1;
	while (isspace (line [i]))
	    i += 1;
	type = line [i];
	do
	    i += 1;
	while (isspace (line [i]));
	ident = line + i;
	i = 0;
	while (! isspace (ident [i]))
	    i += 1;
	ident [i] = '\0';

	if (O->size >= O->alloc)
	    realloc_objet (O, 2 * O->alloc + 1);
	if (ident [0] != '_')
	{   set_symbole_tic (&O->tab [O->size], type, ident, -1);
	    O->size += 1;
	}
	line = fgets (buffer, BUFSIZE-1, f);	
    }
}

/**
 * @brief Affecte P à O. Tous les champs sont dupliqués.
 * @param[out] O un objet
 * @param[in] P un objet
 */

void set_objet (struct objet* O, struct objet* P)
{   int i;

    O->filename = (char*)realloc (O->filename, strlen (P->filename) + 1);
    if (O->filename == (char*)0)
        error ("set_objet", __FILE__, __LINE__);
    strcpy (O->filename, P->filename);

    realloc_objet (O, P->size);

    O->size = 0;
    for (i = 0; i < P->size; i++)
    {	set_symbole (&O->tab [i], &P->tab [i]);
	O->size = i + 1;
    }
}

/**
 * @brief Retourne \p true si \p filename désigne un fichier objet,
 * \p false sinon. 
 * @param[in] filename un nom de fichier
 */

bool est_fichier_objet (char* filename)
{   int l = strlen (filename);
    FILE* f;

    if (l <= 2 || filename [l-1] != 'o' || filename [l-2] != '.')
	return false;
    f = fopen (filename, "r");
    if (f != (FILE*)0)
	fclose (f);
    return f != (FILE*)0;
}

/**
 * @brief Retourne \p true si \p filename désigne une bibliothèque (une 
 * archive), \p false sinon. 
 * @param[in] filename un nom de fichier
 */

bool est_fichier_bibliotheque (char* filename)
{   int l = strlen (filename);
    FILE* f;

    if ((l <= 2 || filename [l-1] != 'a' || filename [l-2] != '.') &&
        (l <= 3 || filename [l-1] != 'o' || filename [l-2] != 's' 
						|| filename [l-3] != '.'))
	    return false;
    f = fopen (filename, "r");
    if (f != (FILE*)0)
	fclose (f);
    return f != (FILE*)0;
}
