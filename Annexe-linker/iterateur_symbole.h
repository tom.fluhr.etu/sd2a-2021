#if ! defined (ITERATEUR_SYMBOL_H)
#define ITERATEUR_SYMBOLE_H 1

#include <stdbool.h>
#include "symbole.h"
#include "objet.h"

/**
 * @file
 * @brief
 * Les types nécessaires à l'énumération des symboles des fichiers
 * objets et des bibliothèques.
 * @author F. Boulier
 * @date novembre 2010
 */

/***********************************************************************
 * IMPLANTATION
 ***********************************************************************/

/**
 * @struct iterateur_symbole
 * @brief Le type \p struct \p iterateur_symbole permet d'énumérer les symboles
 * d'un fichier objet ou d'une bibliothèque (une archive).
 * Le champ \p filename contient le nom du fichier. 
 * Le champ \p f contient un descripteur de fichier, sur lequel les
 * symboles sont lus.
 * Le champ \p uniquement_objet permet, lorsque le fichier à parcourir
 * est une bibliothèque, à limiter l'énumération aux seuls symboles du
 * fichier objet courant.
 * Le champ \p est_objet permet d'indiquer si le fichier parcouru est
 * un fichier objet ou une bibliothèque.
 * Le champ \p objet contient les symboles du fichier objet courant.
 * Le champ \p i est un indice dans objet.tab.
 * La chaîne \p filename ainsi que les chaînes présentes dans les symboles
 * de \p objet sont allouées dynamiquement.
 */

struct iterateur_symbole
{   char* filename;         /**< le nom du fichier */
    FILE* f;                /**< les données sont lues sur f */
    bool est_objet;         /**< le fichier parcouru est un objet */
    bool uniquement_objet;  /**< si ce n'est pas un objet, faut-il ne
				 parcourir que l'objet courant */
    struct objet objet;     /**< les symboles de l'objet courant */
    int i;                  /**< un indice dans objet.tab */
};

/***********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 ***********************************************************************/

extern struct symbole* first_symbole (struct iterateur_symbole*, char*);

extern struct symbole* first_symbole_objet_courant 
		(struct iterateur_symbole*, struct iterateur_symbole*);

extern void clear_iterateur_symbole (struct iterateur_symbole*);

extern struct symbole* next_symbole (struct iterateur_symbole*);
extern struct symbole* next_symbole_next_objet (struct iterateur_symbole*);

#endif
