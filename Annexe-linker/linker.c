#include <stdlib.h>
#include <stdio.h>
#include "symtable.h"
#include "iterateur_symbole.h"
#include "error.h"

/**
 * @file
 * @brief Implantation simplifiée de l'algorithme du linker.
 * @author F. Boulier
 * @date novembre 2010
 */

/**
 * @brief Applique l'algorithme du linker sur les fichiers objets
 * et les bibliothèques passés sur la ligne de commande.
 * En fait, l'algorithme se contente d'indiquer si l'édition des
 * liens est possible ou si certains symboles restent indéfinis
 * ou encore si certains symboles sont définis plusieurs fois.
 * Tous les symboles des fichiers objets sont incorporés dans 
 * l'exécutable. Pour les fichiers objets présents dans les
 * bibliothèques, seuls sont chargés ceux qui définissent un
 * symbole indéfini, au moment où la bibliothèque est parcourue.
 * @param[in] argc le nombre de paramètres de la ligne de commande
 * @param[in] argv les paramètres de la ligne de commande
 */

int main (int argc, char** argv)
{   struct symtable table;
    struct iterateur_symbole iter, iter2;
    struct symbole *sym, *sym2;
    int i, status;
    bool reloop;

    init_symtable (&table);
    i = 1;
    while (i < argc)
    {	commenter_stats (&table.mesures, argv [i]);
	reloop = false;
	sym = first_symbole (&iter, argv [i]);
	if (est_fichier_objet (argv [i]))
	{   
/* Charge tous les symboles de l'objet */
	    while (sym != (struct symbole*)0)
	    {	if (! est_local_symbole (sym))
		    enregistrer_dans_symtable (&table, sym);
		sym = next_symbole (&iter);
	    }
	} else
	{   
/* 
 * On parcourt toute la bibliothèque à la recherche d'une définition
 * pour un symbole indéfini.
 */
	    while (sym != (struct symbole*)0)
	    {	if (est_global_et_defini_symbole (sym))
		{   sym2 = rechercher_dans_symtable (sym->ident, &table);
		    if (sym2 != (struct symbole*)0 && 
						est_indefini_symbole (sym2))
		    {	
/* On en a trouvé un : on charge l'objet auquel ce symbole appartient */
			sym2 = first_symbole_objet_courant (&iter2, &iter);
			while (sym2 != (struct symbole*)0)
			{   if (! est_local_symbole (sym2))
				enregistrer_dans_symtable (&table, sym2);
			    sym2 = next_symbole (&iter2);
			}
			clear_iterateur_symbole (&iter2);
/* On devra parcourir à nouveau la bibliothèque */
			reloop = true;
/* 
 * On saute tous les symboles qui suivent sym dans le fichier objet de sym
 * puisqu'on vient juste de les charger dans la table.
 */
			sym = next_symbole_next_objet (&iter);
		    } else
			sym = next_symbole (&iter);
		} else
		    sym = next_symbole (&iter);
	    }
	}
	clear_iterateur_symbole (&iter);
	if (!reloop)
	    i += 1;
    }
/* Indique si l'édition des liens est réussie ou pas */
    status = synthese_symtable (&table);
    clear_symtable (&table);

    return status;
}
