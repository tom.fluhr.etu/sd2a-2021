/* chaine.h */

struct chaine {
    char* s;
    int i;
    int n;
};

/*
 * Le type struct chaine implante des chaînes redimensionnables
 * s pointe vers un tableau de n char alloués dynamiquement
 * n = le nombre de char alloués à s
 * i = le nombre de char utilisés dans s (on a i < n)
 * 
 *  Rq : on a probablement intérêt à maintenir s avec un '\0' final
 *       en permanence.
 */


