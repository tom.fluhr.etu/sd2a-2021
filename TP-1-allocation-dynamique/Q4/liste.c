/* liste.c */

#include <stdio.h>  // pour printf
#include <stdlib.h> // pour malloc et free
#include "liste.h"

/* Constructeur - Initialise L (mode résultat) à la liste vide */
void init_liste (struct liste* L)
{
    L->tete = NIL;
    L->nbelem = 0;
}

/* Destructeur */
void clear_liste (struct liste* L)
{   struct maillon* M;

    M = L->tete;
    while (M != NIL)
    {   struct maillon* S;

        S = M->suivant;
        free (M);
        M = S;
    }
}

/* Ajoute d (mode donnée) en tête de L (mode donnée-résultat) */
void ajout_en_tete_liste (struct liste* L, double d)
{   struct maillon* M;

    M = malloc (sizeof (struct maillon));
    M->valeur = d;
    M->suivant = L->tete;
    L->tete = M;
    L->nbelem += 1;
}

void ajout_en_queue_liste (struct liste* L, double d)
{
    if (L->tete == NIL)
        ajout_en_tete_liste (L, d);
    else
    {   struct maillon *M;
        struct maillon *N;

        N = malloc (sizeof (struct maillon));
        N->valeur = d;
        N->suivant = NIL;

        M = L->tete;
        while (M->suivant != NIL)
            M = M->suivant;

        M->suivant = N;
        L->nbelem += 1;
    }
}

void print_liste (struct liste L)
{   struct maillon* M;

    M = L.tete; 
    while (M != NIL)
    {   
        printf ("%lf ", M->valeur);
        M = M->suivant;
    }
    printf ("\n");
}



