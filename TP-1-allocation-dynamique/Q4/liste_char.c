/* liste.c */

#include <stdio.h>  // pour printf
#include <stdlib.h> // pour malloc et free
#include "liste_char.h"

/* Constructeur - Initialise L (mode résultat) à la liste vide */
void init_liste (struct liste* L)
{
    L->tete = NIL;
    L->nbelem = 0;
}

/* Destructeur */
void clear_liste (struct liste* L)
{   struct maillon* M;

    M = L->tete;
    while (M != NIL)
    {   struct maillon* S;

        S = M->suivant;
        free (M);
        M = S;
    }
}

/* Ajoute d (mode donnée) en tête de L (mode donnée-résultat) */
void ajout_en_tete_liste (struct liste* L, char* T)
{   struct maillon* M;

    M = malloc (sizeof (struct maillon));
    M->valeur = T;
    M->suivant = L->tete;
    L->tete = M;
    L->nbelem += 1;
}


void print_liste (struct liste L)
{   struct maillon* M;

    M = L.tete; 
    while (M != NIL)
    {   
        printf ("%s ", M->valeur);
        M = M->suivant;
    }
    printf ("\n");
}
