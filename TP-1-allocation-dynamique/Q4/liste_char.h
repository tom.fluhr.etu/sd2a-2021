/*struct maillon {
	char* S;
	struct maillon* suivant;
};


struct liste {
	struct mailon* tete;
	int nbelemnt;
};


extern void ajout_en_tete_liste_char (struct liste*,char* T[]);
extern void print_liste_char (struct liste*);
extern void clear_list_char (struct liste*);
extern void init_liste(struct liste*);*/

/* liste.h */

struct maillon {
    char* valeur;
    struct maillon* suivant;
};

#define NIL (struct maillon*)0

struct liste {
    struct maillon* tete;
    int nbelem;
};

/* Constructeur */
extern void init_liste (struct liste*);

/* Destructeur 
extern void clear_liste (struct liste);*/

/* Ajoute d (mode donnée) en tête de L (mode donnée-résultat) */
extern void ajout_en_tete_liste (struct liste* L,char* T);

extern void print_liste (struct liste);

