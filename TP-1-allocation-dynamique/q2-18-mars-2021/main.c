/* main.c */

#include <stdio.h>
#include <ctype.h>
#include "chaine.h"

int main ()
{
  int c;
  struct chaine strng;

  init_chaine (&strng);
  c = getchar ();
  while (!isspace (c))
    {
      ajout_en_queue_chaine (&strng, (char)c);
      c = getchar ();
    }
  print_chaine (&strng);
  clear_chaine (&strng);
  return 0;
}
