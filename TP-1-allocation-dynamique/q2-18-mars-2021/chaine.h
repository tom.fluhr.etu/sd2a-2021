/* chaine.h */

struct chaine {
    char* s; // pointe sur la zone allouée dynamiquement
    int n;   // le nombre de char alloués à s
    int i;   // le nombre de char utilisés dans s
};

extern void init_chaine (struct chaine*);

extern void ajout_en_queue_chaine (struct chaine*, char);

extern void print_chaine (struct chaine*);

extern void clear_chaine (struct chaine*);
