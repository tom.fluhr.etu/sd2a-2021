/* chaine.c */

#include <stdlib.h> // malloc
#include <stdio.h> // printf
#include "chaine.h"

void init_chaine (struct chaine* strng)
{
    /* initialiser la chaine avec un tableau de 4 char dans s */
    strng->i = 0;
    strng->n = 4;
    strng->s = malloc (strng->n * sizeof(char));
    strng->s[strng->i] = '\0'; /* chaîne vide dans strng->s */
}

void ajout_en_queue_chaine (struct chaine* strng, char c)
{
    /* on rajoute juste 1 char dans s --- on peut être amené à réallouer s */
    if (strng->i == strng->n - 1)
    {   strng->n += 8;
        strng->s = realloc (strng->s, strng->n * sizeof(char));
    }
    strng->s[strng->i] = c;
    strng->i += 1;
    strng->s[strng->i] = '\0';
}

void print_chaine (struct chaine* strng)
{
    /* interdiction de modifier strng qui est passé en mode donnée */
    printf ("%s\n", strng->s);
}

void clear_chaine (struct chaine* strng)
{
    free (strng->s);
}

