/* rationnel.c */

#include <stdlib.h> // pour malloc et free
#include <stdio.h>
#include "rationnel.h"

/* retourne le pgcd de a0 et b0 */

static int Euclide (int a0, int b0)
{   int a, b, t;

    if (a0 > 0) a = a0; else a = - a0;
    if (b0 > 0) b = b0; else b = - b0;
    while (b != 0)
    {   t = a % b;   /* le reste de la division de a par b */
        a = b;
        b = t;
    }
    return a;
}

/*
 * Constructeur. Donc R->data n'est pas initialisé. Donc malloc 
 */

void init_rationnel (struct rationnel* R, int a, int b)
{
    R->data = malloc (2*sizeof(int));
    set_rationnel (R, a, b);
}

/*
 * Pas un constructeur. Affecte a/b à R
 */

void set_rationnel (struct rationnel* R, int a, int b)
{   int p;

    p = Euclide (a, b);
    if (b > 0)
    {   R->data[0] = a/p;
        R->data[1] = b/p;
    } else
    {   R->data[0] = -a/p;
        R->data[1] = -b/p;
    }
}

/*
 * Destructeur : c'est la dernière fois qu'on voit A
 * C'est ici qu'i faut faire faire un free
 */

void clear_rationnel (struct rationnel* A)
{
    free (A->data);
}

void print_rationnel (struct rationnel A)
{
    printf ("%d/%d\n", A.data[0], A.data[1]);
}

/* 
 * Aaargh : il y a un bug - qui se verra aujourd'hui
 * Maintenant corrigé.
 */

/*
 * Pas un constructeur donc R est initialisé. 
 * Donc pas de malloc (déjà fait)
 */

void add_rationnel
        (struct rationnel* R, struct rationnel* A, struct rationnel* B)
{
    int p, q;
    p = A->data[0] * B->data[1] + A->data[1] * B->data[0];
    q = A->data[1] * B->data[1];
    set_rationnel (R, p, q);
        /* on ne devrait pas appliquer un constructeur sur R puisque R 
           est déjà initialisé. si on le fait : fuite mémoire */
}


