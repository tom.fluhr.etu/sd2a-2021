#include <stdio.h>
#include <string.h>
#include <stdlib.h> /* pour malloc et free */

int main ()
{   char* T;
    int d;

    printf ("entrez la taille : ");
    scanf ("%d", &d);
    T = malloc (d * sizeof(char)); /* alloué dans le tas */
    strcpy (T, "elephant");
    printf ("%s\n", T);
    free (T); /* on restitue les 10 char au tas */
    return 0;
}

