/* main.c */

#include <stdio.h>
#include "liste.h"

int main ()
{   double T [] = { 173, 25, -9 };
    int n = sizeof(T)/sizeof(double);

    struct liste L0;
    init_liste (&L0);  /* initialise L0 à la liste vide */
    for (int i = 0; i < n; i++)
    {
        ajout_en_tete_liste (&L0, T[i]);
    }
    print_liste (L0);
    clear_liste (&L0);
    return 0;
}


