/* liste.h */

struct maillon {
    double valeur;
    struct maillon* suivant;
};

#define NIL (struct maillon*)0

struct liste {
    struct maillon* tete;
    int nbelem;
};

/* Constructeur */
extern void init_liste (struct liste*);

/* Destructeur */
extern void clear_liste (struct liste*);

/* Ajoute d (mode donnée) en tête de L (mode donnée-résultat) */
extern void ajout_en_tete_liste (struct liste* L, double d);

extern void print_liste (struct liste);


