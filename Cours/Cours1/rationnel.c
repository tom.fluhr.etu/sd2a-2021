/* rationnel.c */

#include <stdio.h>
#include "rationnel.h"


/* retourne le pgcd de a0 et b0 */

static int Euclide (int a0, int b0)
{   int a, b, t;

    if (a0 > 0) a = a0; else a = - a0;
    if (b0 > 0) b = b0; else b = - b0;
    while (b != 0)
    {   t = a % b;   /* le reste de la division de a par b */
        a = b;
        b = t;
    }
    return a;
}

void init_rationnel (struct rationnel* R, int a, int b)
{   int p;

    p = Euclide (a, b);
    if (b > 0)
    {   R->numer = a/p;
        R->denom = b/p;
    } else
    {   R->numer = -a/p;
        R->denom = -b/p;
    }
}

void clear_rationnel (struct rationnel* A)
{
}

void print_rationnel (struct rationnel A)
{
    printf ("%d/%d\n", A.numer, A.denom);
}

/* 
 * Aaargh : il y a un bug - mais qui ne se verra pas aujourd'hui
 */

void add_rationnel
        (struct rationnel* R, struct rationnel* A, struct rationnel* B)
{
    int p, q;
    p = A->numer * B->denom + A->denom * B->numer;
    q = A->denom * B->denom;
    init_rationnel (R, p, q); /* on ne devrait pas appliquer un
        constructeur sur R puisque R est déjà initialisée */
}


