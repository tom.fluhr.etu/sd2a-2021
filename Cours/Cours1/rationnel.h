/* rationnel.h */

struct rationnel {
    int numer;
    int denom;
};

/*
 * Spécifications du type
 * Le champ denom est strictement positif
 * Le signe est porté par le numérateur
 * La fraction est réduite : le pgcd des champs numer et denom vaut 1
 */

/*
 * Constructeur. Affecte à R (mode résultat) la fraction a/b. 
 */
extern void init_rationnel (struct rationnel* R, int a, int b);

/*
 * Destructeur (mode donnée-résultat). 
 */
extern void clear_rationnel (struct rationnel*);

/*
 * Affichage (mode donnée). On pourrait faire un passage par adresse.
 *                          Pour varier un peu, on laisse un passage par valeur
 */

extern void print_rationnel (struct rationnel);

/*
 * Affecte A + B à R (R en mode résultat ; A et B en mode donnée)
 */

extern void add_rationnel 
        (struct rationnel* R, struct rationnel* A, struct rationnel* B);


