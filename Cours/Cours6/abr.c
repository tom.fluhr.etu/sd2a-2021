#include <stdlib.h>
#include <stdio.h>
#include "abr.h"

bool recherche_abr (double d, struct noeud* A0)
{
    bool found = false;
    struct noeud* A;

    A = A0;
    while (A != NIL && !found)
    {   if (A->valeur == d)
            found = true;
        else if (A->valeur < d)
            A = A->droit;
        else
            A = A->gauche;
    }
    return found;
}

/*
bool recherche_abr (double d, struct noeud* A0)
{
    if (A0 == NIL)
        return false;
    else if (A0->valeur == d)
        return true;
    else if (A0->valeur < d)
        return recherche_abr (d, A0->droit);
    else
        return recherche_abr (d, A0->gauche);
}
*/

static struct noeud* new_feuille (double d)
{   struct noeud* F;
    F = malloc (sizeof (struct noeud));
    F->gauche = NIL;
    F->valeur = d;
    F->droit = NIL;
    return F;
}

struct noeud* ajout_abr (struct noeud* A0, double d)
{
    if (A0 == NIL)
        return new_feuille (d);
    else
    {   struct noeud* cour;
        struct noeud* prec;
        cour = A0;
        while (cour != NIL)
        {
            prec = cour;
            if (cour->valeur < d)
                cour = cour->droit;
            else
                cour = cour->gauche;
        }
        if (prec->valeur < d)
            prec->droit = new_feuille (d);
        else
            prec->gauche = new_feuille (d);
        return A0;
    }
}

/*
struct noeud* ajout_abr (struct noeud* A, double d)
{
    if (A == NIL)
        return new_feuille (d);
    else if (A->valeur < d)
        A->droit = ajout_abr (A->droit, d);
    else
        A->gauche = ajout_abr (A->gauche, d);
    return A;
}
*/


static void affichage_abr2 (struct noeud* A)
{
    if (A != NIL)
    {   affichage_abr2 (A->gauche);
        printf ("%lf ", A->valeur);
        affichage_abr2 (A->droit);
    }
}

void affichage_abr (struct noeud* A)
{
    printf ("A = ");
    affichage_abr2 (A);
    printf ("\n");
}







