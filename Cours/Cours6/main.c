#include <stdio.h>
#include "abr.h"

int main ()
{   double T [] = { 17, 40, 37, 5, 60, 35 };
    int n = sizeof(T)/sizeof(double);

    struct noeud* A0;

    A0 = NIL;
    for (int i = 0; i < n; i++)
    {   A0 = ajout_abr (A0, T[i]);
        affichage_abr (A0);
    }
    if (recherche_abr (35, A0))
        printf ("35 dans l'ABR\n");
    else
        printf ("35 n'est pas dans l'ABR\n");
    return 0;
}

