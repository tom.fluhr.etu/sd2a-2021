/* abr.h */

struct noeud {
    struct noeud* gauche;
    double valeur;
    struct noeud* droit;
};

#define NIL (struct noeud*)0

#include <stdbool.h>

extern bool recherche_abr (double, struct noeud*);

extern struct noeud* ajout_abr (struct noeud*, double);

extern void affichage_abr (struct noeud*);
