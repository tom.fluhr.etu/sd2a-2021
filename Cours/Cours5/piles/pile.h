/* pile.h */

/* Use the code, Luke ! */

#define N 3

struct pile {
    double T [N];
    int sp;
};

#include <stdbool.h>

/* Constructeur. Initialise à la pile vide */
extern void init_pile (struct pile*);

extern void clear_pile (struct pile*);

extern void empiler (struct pile*, double);

extern double depiler (struct pile*);

extern bool est_vide_pile (struct pile*);


