#include <stdio.h>
#include "pile.h"

int main ()
{   double data [] = { 23, 47, 108, -9 } ;
    int n = sizeof(data)/sizeof(data[0]);

    struct pile P0;

    init_pile (&P0);
    for (int i = 0; i < n; i++)
    {
        printf ("on empile %lf\n", data[i]);
        empiler (&P0, data[i]);
    }
    while (! est_vide_pile (&P0))
    {   double d;
        d = depiler (&P0);
        printf ("on depile %lf\n", d);
    }
    clear_pile (&P0);
    return 0;
}

