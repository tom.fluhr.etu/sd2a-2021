/* pile.c */

#include <assert.h>
#include "pile.h"

void init_pile (struct pile* P)
{
    P->sp = 0;
}

void clear_pile (struct pile* P)
{
}

static bool est_pleine_pile (struct pile* P)
{
    return P->sp == N;
}

void empiler (struct pile* P, double d)
{
    assert (! est_pleine_pile (P));
    P->T [P->sp] = d;
    P->sp += 1;
/*
  Ou encore :
    P->T [P->sp++] = d;
 */
}

double depiler (struct pile* P)
{
    assert (! est_vide_pile (P));
    P->sp -= 1;
    return P->T [P->sp];
/*
   Ou encore :
    return P->T[--P->sp];
 */
}

bool est_vide_pile (struct pile* P)
{
    return P->sp == 0;
}

