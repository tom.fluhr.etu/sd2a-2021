/* file.c */

#include <assert.h>
#include "file.h"

void init_file (struct file* F)
{
    F->re = 0;
    F->we = 0;
}

void clear_file (struct file* F)
{
}

static bool est_pleine_file (struct file* F)
{
    return (F->we + 1) % N == F->re;
}

void enfiler (struct file* F, double d)
{
    assert (! est_pleine_file (F));
    F->T [F->we] = d;
    F->we = (F->we + 1) % N;
}

double defiler (struct file* F)
{   double d;
    assert (! est_vide_file (F));
    d = F->T [F->re];
    F->re = (F->re + 1) % N;
    return d;
}

bool est_vide_file (struct file* F)
{
    return F->re == F->we;
}

