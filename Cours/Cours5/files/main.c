#include <stdio.h>
#include "file.h"

int main ()
{   double data [] = { 23, 47, 108, -9 } ;
    int n = sizeof(data)/sizeof(data[0]);

    struct file F0;

    init_file (&F0);
    for (int i = 0; i < n; i++)
    {
        printf ("on enfile %lf\n", data[i]);
        enfiler (&F0, data[i]);
    }
    while (! est_vide_file (&F0))
    {   double d;
        d = defiler (&F0);
        printf ("on defile %lf\n", d);
    }
    clear_file (&F0);
    return 0;
}

