/* file.h */

/* Use the code, Luke ! */

#define N 10

struct file {
    double T [N];
    int re;
    int we;
};

#include <stdbool.h>

/* Constructeur. Initialise à la file vide */
extern void init_file (struct file*);

extern void clear_file (struct file*);

extern void enfiler (struct file*, double);

extern double defiler (struct file*);

extern bool est_vide_file (struct file*);


