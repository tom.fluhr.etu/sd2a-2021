#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include "liste_wstring.h"
#include <wchar.h>

void init_liste_wstring (struct liste_wstring* L)
{
    L->tete = (struct maillon_wstring*)0;
    L->nbelem = 0;
}

void ajouter_en_tete_liste_wstring (struct liste_wstring * L, wstring c,wstring sat)
{   struct maillon_wstring* nouveau;

    nouveau = (struct maillon_wstring*)malloc (sizeof (struct maillon_wstring));
    assert (nouveau != (struct maillon_wstring*)0);
/* appeler ici un éventuel constructeur pour nouveau->value */
    wcscpy(nouveau->clef,c);
    wcscpy(nouveau->satellite,sat); 
    nouveau->next = L->tete;
    L->tete = nouveau;
    L->nbelem += 1;
}


void clear_liste_wstring (struct liste_wstring * L)
{   struct maillon_wstring* courant;
    struct maillon_wstring* suivant;
    int i;

    courant = L->tete;
    for (i = 0; i < L->nbelem; i++)
    {   suivant = courant->next;
        free (courant);
        courant = suivant;
    }
}


void imprimer_liste_wstring (struct liste_wstring * L)
{   struct maillon_wstring* M;
    int i;

    wprintf (L"[");
    M = L->tete;
    for (i = 0; i < L->nbelem; i++)
    {   if (i == 0)
            wprintf (L"%ls, %ls \n",M->clef,M->satellite);
        else
            wprintf (L"%ls, %ls \n",M->clef,M->satellite);
        M = M->next;
    }
    wprintf (L"]\n");
}


wchar_t * rechercher_liste_wstring(struct liste_wstring * L,wstring x){
struct maillon_wstring* M;
M=L->tete;
bool trouve=false;
    while(M!=(struct maillon_wstring*)0 && trouve==false){
        if(wcscmp(M->clef,x)==0){
            trouve=true;
        }
        M=M->next;
    }
    return M->clef;
}
