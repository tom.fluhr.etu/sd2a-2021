#include "hachage_simple.h"
#include <stdbool.h>
#include <stdio.h>
#include <wchar.h>


void init_table(struct table* T, fonction_hachage* h){
    int i;
     for(i=0;i<N;i++){
         init_liste_wstring(&T->tab[i].L);
     }
     T->hash=h;
    }
     
     
void enregistrer_table(struct table* T,wstring x,wstring y){
    int i;
    i=T->hash(x);
    ajouter_en_tete_liste_wstring(&T->tab[i].L,x,y);
}

void clear_table(struct table* T){
    int i;
     for (i=0;i<N;i++){
         clear_liste_wstring(&T->tab[i].L);
     }
}

bool rechercher_table (struct table* T, wstring x){
    int i;
    i=T->hash(x);
    return rechercher_liste_wstring(&T->tab[i].L,x);
    
}


void imprimer_table(struct table* T){
    int i;
     for (i=0;i<N;i++){
         imprimer_liste_wstring(&T->tab[i].L);
     }
}
    
