#include <stdlib.h>

struct point {
    double x;
    double y;
    char ident;
};

/*
 * Cette fonction est appelée par qsort
 * Si on retourne -1 => a doit être rangé avant b
 *                 1 => a doit être rangé après b
 *                 0 => on a égalité entre a et b
 */

int fonction_de_comparaison (const void* a0, const void* b0)
{   struct point *a = (struct point*)a0;    
                            // crois-moi : a0 est un struct point*
    struct point *b = (struct point*)b0;
    double xu, yu, xv, yv, det;

    xu = a->x;
    yu = a->y;
    xv = b->x;
    yv = b->y;
    det = xu*yv - xv*yu;

    if (det > 0)
        return -1;
    else
        return 1;
}

int main ()
{   struct point tab [] = { { 10, 2, 'S' }, 
                            { -1, 7, '3' },
                            { 10, 0, 'I' },
                            { 5, 5, 'A' },
                            { 8, 3, '2' } };
    int n = sizeof(tab)/sizeof(struct point);

    qsort (tab,             // l'adresse du tableau à trier
           n,               // le nombre d'éléments du tableau
           sizeof(struct point),  // la taille de chaque élément du tableau
           &fonction_de_comparaison);   // l'adresse de la fonction
                            // à utiliser pour comparer deux elts
    return 0;
}

