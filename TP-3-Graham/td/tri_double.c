#include <stdlib.h>

/*
 * Cette fonction est appelée par qsort
 * Si on retourne -1 => a doit être rangé avant b
 *                 1 => a doit être rangé après b
 *                 0 => on a égalité entre a et b
 */

int fonction_de_comparaison (const void* a0, const void* b0)
{   double *a = (double*)a0;    // crois-moi : a0 est un double*
    double *b = (double*)b0;

    if (*a < *b)
        return -1;
    else
        return 1;
}

int main ()
{   double tab [] = { 108, -3, 51, 44, 1099 };
    int n = sizeof(tab)/sizeof(double);

    qsort (tab,             // l'adresse du tableau à trier
           n,               // le nombre d'éléments du tableau
           sizeof(double),  // la taille de chaque élément du tableau
           &fonction_de_comparaison);   // l'adresse de la fonction
                            // à utiliser pour comparer deux elts
    return 0;
}

