/* Graham.c */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include "liste_point.h"

#define N 10
#define MAX_COORDINATES 100
#define SCENARIO 78738

int main ()
{   struct point T[N];
    struct liste_point L;
    FILE* f;
    int i;

    srand48 (SCENARIO);
/* 
 * On crée N points. Le point A est en (0,0).
 * On les enregistre dans "points.dat"
 */
    init_point (&T[0], 0, 0, 'A');
    for (i = 1; i < N; i++)
        init_point (&T[i], drand48 () * MAX_COORDINATES,
                           drand48 () * MAX_COORDINATES, 'A' + i);
    f = fopen ("points.dat", "w");
    assert (f != NULL);
    for (i = 0; i < N; i++)
        fprintf (f, "%f %f %c\n", T[i].x, T[i].y, T[i].ident);
    fclose (f);
/* 
 * On trie T [1 .. N-1] par angle croissant.
 * On s'assure qu'il n'y a pas deux points alignés.
 */
    qsort (T+1, N-1, sizeof (struct point), &compare_points);
    for (i = 1; i < N-1; i++)
        assert (compare_points (&T[i], &T[i+1]) != 0);
    for (i = 0; i < N; i++)
        printf ("%c ", T[i].ident);
    printf ("\n");
/*
 * À FAIRE : BOUCLE PRINCIPALE DE L'ALGORITHME DE GRAHAM.
 *           UTILISER L POUR LA PILE DE POINTS.
 */

f = fopen ("enveloppe.dat", "w"); 
init_liste_point (&L);
ajouter_en_tete_liste_point (&L, T[0]);
fprintf (f, "%f %f %c\n", L.tete->value.x, L.tete->value.y, L.tete->value.ident);
struct point prec[100];
struct point prec_2;
prec[0]=T[0];
imprimer_liste_point (&L); 
ajouter_en_tete_liste_point (&L, T[1]);
fprintf (f, "%f %f %c\n", L.tete->value.x, L.tete->value.y, L.tete->value.ident);
imprimer_liste_point (&L); 
i =2;
int j =0;
int n = sizeof(T)/sizeof(struct point);
prec_2 = T[0];
while (i<n)
{
/*
	cour = L.tete->value
*/
	if (tourne_a_gauche (&L.tete->value,&prec_2,&T[i])==true){
		prec[i-1]= L.tete->value;
		prec_2=prec[i-1];
		ajouter_en_tete_liste_point (&L, T[i]);
		i = i + 1;
		j=i-2;	
	}else{
		j=j+2;
		prec_2= prec[j-3];
		extraire_tete_liste_point (&L.tete->value, &L);
		prec[i-1]= L.tete->value;
		

}
fprintf (f, "%f %f %c\n", L.tete->value.x, L.tete->value.y, L.tete->value.ident);  
}
fclose (f);
/*EXPLICATION*/
/*
Je n'aarivais pas a print ma lise en utilisant le code que j'ai mis en dessous, jai donc décider de print mon sommet de pile a chaque tour de boucle, je vois donc que le code semble marché en empilant puis dépilant les sommet dans les cas échéant (etape depile H on retrouve bien C en sommet puis empile I...), en supprimant a la main les ligne depiler normalement j'obtient bien dans gnuplot le dessins du sujet.
*/


/*idée d'affaichage voulu mais n'affiche rien (semble compiler), cf : fonction liste_point

f = fopen ("enveloppe.dat", "w"); 
struct maillon_point *M;
M=L.tete;
while (i<(sizeof(L)/sizeof(struct point))){
fprintf (f, "%f %f %c\n", M->value.x, M->value.y, M->value.ident);
M = M->next;
i=i+1;
}
fclose (f);
*/
 
    
/* 
 * Commande Gnuplot:
 * plot [-10:100][-10:100] "points.dat" with labels, "enveloppe.dat" with lines
 */
    return 0;
}
