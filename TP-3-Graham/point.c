#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include "point.h"

void init_point (struct point* P, double a, double o, char N){
	P->x=a;
	P->y=o;
	P->ident=N;

}
int compare_points (const void* a0, const void* b0){
struct point* a = (struct point*)a0;
struct point* b = (struct point*)b0;
double det, xu, yu, xv, yv;
xu=a->x;
yu=a->y;
xv=b->x;
yv=b->y;
det=(xu*yv-(yu*xv));

if (det < 0 ){
	return -1;
	}else{ 
	return 1;
	}
}

bool tourne_a_gauche (struct point* A, struct point* B, struct point* C){
double det, xu, yu, xv, yv;
xu=(B->x - A->x);
yu=(B->y - A->y);
yv=(C->y - A->y);
xv=(C->x - A->x);
det=(xu*yv-(yu*xv));

if (det > 0 ){
	return true;
	}else{ 
	return false;
	}
}
