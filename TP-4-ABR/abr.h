#include <stdbool.h>

struct abr{
	struct abr* droite;
	int valeur;
	struct abr* gauche;
};

#define NIL (struct abr*)0

extern struct abr* ajout_abr(struct abr*,int x);
extern void afficher_abr(struct abr*);
extern void clear_abr(struct abr*);
extern int hauteur_abr(struct abr*);
extern int nombre_noeud_abr(struct abr*);
extern bool recherche_abr(struct abr*, int x);

/* explication */
/*
 */



