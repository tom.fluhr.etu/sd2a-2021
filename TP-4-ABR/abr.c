#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include "abr.h"


static struct abr *
new_abr (int x)
{
  struct abr *A;
  A = malloc (sizeof (struct abr));
  A->droite = NIL;
  A->gauche = NIL;
  A->valeur = x;
  return A;
}

struct abr *
ajout_abr (struct abr *A, int x)
{
  if (A == NIL)
    return new_abr (x);
  else if (A->valeur < x)
    A->droite = ajout_abr (A->droite, x);
  else
    A->gauche = ajout_abr (A->gauche, x);
  return A;

}

/*struct abr* ajout_abr(struct abr*A, int d){
struct abr *NV;
NV = malloc(sizeof(struct abr));
NV->droite = NIL;
NV->gauche = NIL;
NV->valeur = d;

if (A==NIL)
return NV;
else if(A->valeur < d)
	A-> droite = ajout_abr(A->droite,d);
else 
	A-> gauche = ajout_abr(A->gauche,d);
return A;
}*/

static void
affichage_abrbis (struct abr *A)
{
  if (A != NIL)
    {
      affichage_abrbis (A->gauche);
      printf ("%d ", A->valeur);
      affichage_abrbis (A->droite);
    }
}

void
afficher_abr (struct abr *A)
{
  printf ("la valeur de l'abr est : ");
  affichage_abrbis (A);
  printf ("\n");
}


/*affichage au fromat dot*/

/*static void aff_dot2(FILE* F, struct abr *A)
{
if (A-> gauche != NIL)
	fprintf(f,"%lf -> %lf[label=\"gauche\"];\n",
		A->valeur, A->gauche->valeur);
		aff_dot2(f,A->gauche);
/*code symétrique A-> droite */



/*void aff_dot(struct abr* A)
{ FILE *f;
f = fopen("abr.dot","w");
assert(f != NULL);
fprintf(f,"digraphe G {\n");
if ( A== NIL)
fprintf ("NIL;\n");
else if (est_feuille(A))
	fprintf ("%lf\n",A->valeur);
	else 
	aff_dot2(f,A);
fprintf(f,"}\n");
fclose (f);
system("dot-Tpdf-Grandkdir=LR abr_dot -o abr.pdf");
system("evince abr.pdf")
}

*/

void
clear_abr (struct abr *A)
{
  if (A != NIL)
    {
      clear_abr (A->gauche);
      clear_abr (A->droite);
      free (A);
    }
}

int
hauteur_abr (struct abr *A)
{
  if (A == NIL)
    return -1;

  else
    {
      int hautG = hauteur_abr (A->gauche);
      int hautD = hauteur_abr (A->droite);
      if (hautG > hautD)
	return hautG + 1;
      else
	return hautD + 1;


    }
}

int
nombre_noeud_abr (struct abr *A)
{
  if (A == NIL)
    return 0;
  else
    {
      int i = nombre_noeud_abr (A->gauche);
      int j = nombre_noeud_abr (A->droite);

      return (i + j + 1);
    }
}

bool
recherche_abr (struct abr *A, int d)
{
  if (A == NIL)
    {
      return false;
    }
  else if (d == A->valeur)
    {
      return true;
    }
  else if (d > A->valeur)
    return recherche_abr (A->droite, d);
  else
    return recherche_abr (A->gauche, d);
}

