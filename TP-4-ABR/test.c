#include <stdlib.h>
#include <stdio.h>
#include "abr.h"
#include <stdbool.h>

struct abr* ajout_abr(struct abr* A, int x){
struct abr* NV;
NV=malloc(sizeof(struct abr));
NV->droite = NIL;
NV->gauche = NIL;
NV->valeur = x;

if (A==NIL)
	return NV;
else if (A->valeur <  x)
	A->droite = ajout_abr(A->droite,x);
else 
	A->gauche = ajout_abr(A->gauche,x);
return A;
}

static void afficher_abr2(struct abr* A){
if (A != NIL){
	afficher_abr2(A->gauche);
	printf("%d ",A->valeur);
	afficher_abr2(A->droite);
}
}

void afficher_abr(struct abr*A){
printf("l'abr vaut : ");
afficher_abr2(A);
printf("\n");
}     


int hauteur_abr(struct abr *A){
	if ( A== NIL){
		return -1;
}else{
int hg = hauteur_abr(A->gauche);
int hd = hauteur_abr(A->droite);
		if (hd>hg)
		return hd+1;
		else 
		return hg+1;
}
}
 

int nombre_noeud_abr(struct abr *A){
	if (A==NIL){
		return 0;
}else{
int i = nombre_noeud_abr(A->gauche);
int j = nombre_noeud_abr(A->droite);

return (i+j+1);
}
}

void
clear_abr (struct abr *A)
{
  if (A != NIL)
    {
      clear_abr (A->gauche);
      clear_abr (A->droite);
      free (A);
    }
}

bool
recherche_abr (struct abr *A, int d)
{
if (A==NIL){
	return false;
}else if (A->valeur == d){
	return true;
}else if (A->valeur < d){
	return recherche_abr(A->droite,d);
}else 
	return recherche_abr(A->gauche,d);
}
	
	
	
	
	
	
	
