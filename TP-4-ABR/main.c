#include <stdio.h>
#include "abr.h"

int main ()
{
struct abr* racine;
int x;
racine = NIL;
scanf ("%d", &x);
while (x != -1)
{
racine = ajout_abr (racine,x);
afficher_abr (racine);
scanf ("%d", &x);
}
printf ("la hauteur de l'ABR est %d\n", hauteur_abr (racine));
printf ("le nombre de noeuds de l'ABR est %d\n",
nombre_noeud_abr (racine));
if (recherche_abr (racine, 5))
        printf ("5 dans l'ABR\n");
    else
        printf ("5 n'est pas dans l'ABR\n");
    return 0;
clear_abr (racine);
return 0;
}

